angular.module("gallery.add")
    .controller("AddController", function($scope, galleryService) {

        $scope.notification = false;

        $scope.sendImage = function(file) {
            $scope.notification = false;
            galleryService.setImage(file)
                .then(function(res) {
                    if(res.data.error_code === 0) {
                        $scope.notification = true;
                    }
                });
        }

    });