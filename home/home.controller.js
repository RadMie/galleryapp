angular.module("gallery.home")
    .controller("HomeController", function($scope, galleryService) {

        galleryService.getImages()
            .then(function(res) {
                $scope.images = res.data;
            });


    });