angular.module("gallery")
    .factory("galleryService", function($http, Upload) {

        var url = "https://impaq.herokuapp.com/upload";

        function getImages() {
            return $http.get(url);
        }

        function setImage(file) {
            return Upload.upload({
                url: url,
                data: { file: file}
            });
        }

        return {
            getImages: getImages,
            setImage: setImage
        }

    });